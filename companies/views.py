# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic import CreateView, ListView
from django.core.urlresolvers import reverse_lazy
# Create your views here.

from companies.models import profile
from companies.forms import profileForm


def home(request):
	return render(request, 'index.html')

class Create(CreateView):
	form_class = profileForm
	template_name = 'companies.html'
	model = profile
	success_url = reverse_lazy('lists')



class Lists(ListView):
	template_name = 'lists.html'
	model = profile
	context_object = 'firstname'