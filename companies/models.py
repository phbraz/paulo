# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class profile(models.Model):
	firstname = models.CharField(max_length=100)
	lastname = models.CharField(max_length=100)
	email = models.EmailField(unique=True)
	phone = models.CharField(max_length=20, blank=True)
	timezone = models.DateTimeField('timezone', auto_now_add=True)
	itens = models.IntegerField()
	company = models.CharField(max_length=100)
	account_name = models.CharField(max_length=100)
	billing_contact = models.CharField(max_length=100)
	tech_contact = models.CharField(max_length=100)
	address_one = models.CharField('address1', max_length=100)
	county = models.CharField(max_length=100)
	city = models.CharField(max_length=100)
	country = models.CharField(max_length=100)
	postcode = models.CharField(max_length=100)
	
	class Meta:
		verbose_name = (u'firstname')


	def __unicode__(self):
		return self.firstname	




